import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import placeholder from "@/assets/images/placeholder.png";

export default class StationList extends React.Component {
  state = {
    persons: [],
  };
  componentDidMount() {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: "http://localhost:8080/api/stations/getData",
      headers: {},
    };

    axios
      .request(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        const persons = response.data;
        this.setState({ persons });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    const handleClick = (persons) => {
      navigate(`/projects/${persons.id}`);
    };
    return (
      <div>
        <ul className="divide-y divide-slate-100 dark:divide-slate-700 -mx-6 -mb-6">
          {this.state.persons.map((person) => (
            <li key={person.id}>
              <Link
                to={"/project/" + person.id}
                className="hover:bg-slate-100 dark:hover:bg-slate-600 dark:hover:bg-opacity-70 hover:text-slate-800 text-slate-600 dark:text-slate-300 block w-full px-4 py-3 text-sm mb-4 last:mb-0 cursor-pointer">
                <div className="flex ltr:text-left rtl:text-right">
                  <div className="flex-none ltr:mr-3 rtl:ml-3">
                    <div className="h-[100px] w-[100px] bg-white dark:bg-slate-700 rounded relative">
                      <img
                        src={placeholder}
                        alt=""
                        className="block w-full h-full object-cover rounded border hover:border-white border-transparent"
                      />
                    </div>
                  </div>
                  <div className="flex-1">
                    <div className="text-slate-800 dark:text-slate-300 text-sm font-medium mb-1`">
                      {person.nama}
                    </div>
                    <div className="text-xs hover:text-[#68768A] font-normal text-slate-600 dark:text-slate-300">
                      {person.nama}
                    </div>
                  </div>
                </div>
              </Link>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}
