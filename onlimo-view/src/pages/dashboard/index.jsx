import React, { useState } from "react";
import Card from "@/components/ui/Card";
import GroupChart1 from "@/components/partials/widget/chart/group-chart-1";
import HomeBredCurbs from "./HomeBredCurbs";
import StationList from "../../components/partials/widget/station-list";

const Dashboard = () => {
  const [filterMap, setFilterMap] = useState("usa");
  return (
    <div>
      {/* <HomeBredCurbs title="Dashboard" /> */}
      <div className="grid grid-cols-12 gap-5 mb-5 justify-center ">
        <div className="col-start-0 lg:col-start-2 "></div>
        <div className="2xl:col-span-8 sm:col-span-8 col-span-8 col-start-">
          <Card bodyClass="p-4">
            <StationList />
          </Card>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
